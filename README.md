# Tasks Supero

Gestão de tarefas da supero, uma versão simplista para gestão de tarefas.

## Insctruções iniciais

Instruções para executar este projeto em seu computador para fins de testes.

### Prerequisites

Você vai precisar de:

```
php-7.0
php7.0-cli
php7.0-intl
mysql-server-5.7
```

### Autor

* **Luiz Paulo Schlischting** - *Initial work* - [luizfloripa](https://github.com/luizfloripa)
