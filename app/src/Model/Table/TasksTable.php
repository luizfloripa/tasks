<?php
/**
 * Created by PhpStorm.
 * User: luiz
 * Date: 04/05/18
 * Time: 17:57
 */

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class TasksTable extends Table
{

    //habilita a convenção do cake para gerar automaticamente a marcação de modified e created
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
    }

    //campos obrigatórios
    public function validationDefault(Validator $validator)
    {

        $validator
            ->notEmpty('titulo');

        return $validator;
    }

}