<?php
/**
 * Created by PhpStorm.
 * User: luiz
 * Date: 05/05/18
 * Time: 11:05
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;

class TasksController extends AppController
{

    public function index(){
        $this->viewBuilder()->setLayout('tasks');
        $tasks = $this->Tasks->find()->all();
        $this->set(compact('tasks'));
    }

    public function addEdit($id = null){
        $this->viewBuilder()->enableAutoLayout(false);
        $this->viewBuilder()->setTemplate('ajax');

        if(!$id){
            if($this->getRequest()->is('post')){
//                var_dump($this->request->getData());
//                die();
                $tasks = $this->Tasks->newEntity();
                $tasks = $this->Tasks->patchEntity($tasks, $this->request->getData());
                $tasks->status = 'Aguardando';
                if ($this->Tasks->save($tasks)) {
                    $resposta['id'] = $tasks->id;
                }
                else{
                    var_dump($tasks->errors());
                }

            }
        }
        $retorno['cards'] = $tasks->toArray();

        $this->set([
            '_serialize' => $retorno
        ]);
    }

    public function getTasks($id = null){
        $this->viewBuilder()->enableAutoLayout(false);
        $this->viewBuilder()->setTemplate('ajax');
        if($id)
            $tasks = $this->Tasks->get()->id($id);
        else
            $tasks = $this->Tasks->find('all',  [
                'order' => ['Tasks.modified' => 'DESC']
            ]);


        $this->set([
            '_serialize' => $tasks->toArray()
        ]);
    }

        public function changeStatus(){

            $id = $this->getRequest()->getData('id');
            $status = $this->getRequest()->getData('status');


        $this->viewBuilder()->enableAutoLayout(false);
        $this->viewBuilder()->setTemplate('ajax');

        if (!$id || !$status){
            $retorno['resposta'] = "Id e status não podem ser nulos";


//            return $retorno['resposta'] = "Id não pode ser nulo";
        }
        else {

//            echo $id;
//            die();

            $tasks = $this->Tasks->get($id);

            if ($tasks) {

                $tasks->status = $status;
                if($this->Tasks->Save($tasks)){
                    $retorno['resposta'] = "Dados salvos com sucesso";
                    $retorno['data'] = $tasks->toArray();
                }
                else{
                    $retorno['resposta'] = "Erro ao atualizar os dados";
                }

            } else {
                $retorno['resposta'] = "Tarefa Não Encontrada!!!";
            }
        }

            $this->set([
                '_serialize' => $retorno
            ]);
    }

    public function delete(){
        $this->viewBuilder()->enableAutoLayout(false);
        $this->viewBuilder()->setTemplate('ajax');
        $id = $this->getRequest()->getData('id');

        if($id){
            $retorno['resposta'] = "Id e status não podem ser nulos";
            $tasks = $this->Tasks->get($id);

            if($tasks){
                if($this->Tasks->delete($tasks)){
                    $retorno['resposta'] = "Tarefa removida com sucesso!!!";
                }
                else{
                    $retorno['resposta'] = "Falha ao remover a tarefa!!!";
                }
            }
            else
                $retorno['resposta'] = "Tarefa Não Encontrada!!!";
        }

        $this->set([
            '_serialize' => @$retorno
        ]);
    }


}