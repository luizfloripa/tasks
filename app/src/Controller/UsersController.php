<?php
/**
 * Created by PhpStorm.
 * User: luiz
 * Date: 04/05/18
 * Time: 17:51
 */

namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validator;

class UsersController extends AppController
{

    public function index(){
        $usuarios = $this->Users->find()->all();
        $this->set(compact('usuarios'));
    }

    public function addEdit($id = null)
    {

        if($this->request->is('post')){

            $user =$this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $this->request->data);
            if($this->Users->save($user)){

                $this->Flash->success('Usuário cadastrado com sucesso');
                return $this->redirect(['action' => 'index']);
            }
            else{
//                var_dump($user->errors());
//                die();
                $this->_errors = $user->errors();
//                $user = new UserForm();
//                $user->setErrors($user->errors());

                $this->Flash->error('Erro ao cadastrar usuário');
            }
        }

        if(is_null($id)){
          $user =$this->Users->newEntity();
        }
        else{

            $this->Users->id = $id;
            $user =$this->Users->get($id);
        }
//        var_dump($user);
        $this->set(compact('user'));
    }

}