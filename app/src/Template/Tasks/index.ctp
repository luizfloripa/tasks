<?php
/**
 * Created by PhpStorm.
 * User: luiz
 * Date: 04/05/18
 * Time: 18:08
 */
?>

<div class="row">

<div id="topDiv">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <form align="center"  action="/tasks/add_edit" id="searchForm">
            <div>
                <input class="form-control" type="text" aria-describedby="tituloHelp" name="titulo" placeholder="Título da tarefa">
            </div>
            <div>
                <textarea class="form-control" id="descricao" name="descricao" rows="3"></textarea>
                <small id="descricaoHelp" class="form-text text-muted">
                    A descrição é opcional.
                </small>
            </div>
            <div>
                <input type="submit" class="btn btn-primary mb-2" value="Salvar">
            </div>

        </div>
    </form>
    </div>



<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


<div class="board" align="center">
    <div id="Aguardando" class="boardColumn boardColumn">
        <header class="boardColumn__header">
            <h1 class="boardColumn__title">Aguardando</h1>
        </header>
            <div class="Aguardando drag boardColumn__taskList">

            </div>
    </div>
    <div id="Iniciada" class="boardColumn boardColumn">
        <header class="boardColumn__header">
            <h1 class="boardColumn__title">Iniciado</h1>
        </header>
        <div class="Iniciada drag boardColumn__taskList">

        </div>
    </div>
    <div id="Concluida" class=" boardColumn boardColumn">
        <header class="boardColumn__header">
            <h1 class="boardColumn__title">Concluido</h1>
        </header>
        <div class="Concluida drag boardColumn__taskList">

        </div>
    </div>
</div>
</div>
</div>



<script>

    var cardList = [];

    cardList['Aguardando'] = [];
    cardList['Iniciada'] = [];
    cardList['Concluida'] = [];

    // $(function(){
    //     $('.drag').draggable({
    //         connectToSortable: '.boardColumn__taskList'
    //     });
    //
    //     $('.drop-container').sortable({
    //         placeholder: 'placeholder',
    //         activate: function(event, ui){
    //             $('.drop-container p').remove();
    //         }
    //     });
    //
    //     $('.lixeira').droppable({
    //         hoverClass: 'lixeira-ativa',
    //         drop: function(event, ui) {
    //             $(ui.draggable).remove();
    //         }
    //     });
    //
    //     $('.salvar').click(function(){
    //         var valores = new Array();
    //
    //         $('.drop-container .drag').each(function(){
    //             valores.push( $(this).html() );
    //         });
    //
    //         alert(valores);
    //     });
    // });

    //muda o status do card
    function atualizaStatus(id,status){
        var url = "/tasks/changeStatus"
        var posting =  $.post( url, {id:id, status:status});
        posting.done(function( data ) {
                console.log(data);
        });
    }

    function deleteCard(id){
        console.log(id);
        var url = "/tasks/delete"
        var posting =  $.post( url, {id:id});
        posting.done(function( data ) {
            $('#card-'+id).remove();
            console.log(data);
        });
    }

    function verificaAlteracao(){
        // var tamanhoAguardando = cardList['Aguardando'].length;
        // for (i = 0; i < tamanhoAguardando ; i++ ){
        //     $('#'+cardList['Aguardando'][i])
        // }

        var coluna;
        var intesColuna;

        intesColuna = $( "div#Iniciada" ).find( "div.taskCard" );


        $.each(intesColuna, function(index, item) {
            // $('.'+item.status).append(populaCard(item));
            // cardList[item.status][item.id] = item;
            // console.log(index);
            console.log(item);

            // if(index){

                // coluna = $('#card-'+index).parent().attr('id');
                // if(coluna != item)
                // console.log(coluna + item);
            //
            // }


        });

        // cardList['Aguardando'].forEach( function(s) {
        //     ... do something with s ...
        // } );

        // for (var i in cardList['Aguardando']) {
        //     console.log(cardList['Aguardando']);
        // }

    }

    $(document).ready(function() {


            //popula cards
            function populaCard(item){
                var card = '            <div data-id="'+item.id+'" id="card-'+item.id+'" class="taskCard  taskCard--MEDIUM">\n' +
                    '                <header class="taskCard__header">\n' +
                    '                    <h4 class="taskCard__title">' + item.titulo + '</h4>\n' +
                    '                    <div class="taskCard__epicLink-wrapper">\n' + item.descricao +
                    '                    </div>\n' +
                    '                </header>\n' +
                    '                 <footer class="taskCard__footer"><span class="taskCard__type">' + item.created + '</span>\n' +
                    '                    <div>\n' +
                    '                        <img class="taskCard__avatar"\n' +
                    '                             src="https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAemAAAAJGFkOGZiYTIwLTU0YjUtNGY0Ni1hM2ZhLThlY2Y2NzU0Mzk3MA.jpg">\n' +
                    '                    <i onclick="if(confirm(\'Deseja Remover o card?\'))deleteCard('+item.id+');" class="fa fa-trash" aria-hidden="true"></i> </div>   \n' +
                    '                </footer>\n' +
                    '            </div>'
                return card;
            }

            //busca cards
            $.get( "/tasks/getTasks", function( data ) {
                var response = jQuery.parseJSON(data);
                $.each(response, function(index, item) {
                    $('.'+item.status).append(populaCard(item));
                    cardList[item.status][item.id] = item.status;
                });
            });



    // Attach a su bmit handler to the form
    $("#searchForm").submit(function( event ) {

        // Stop form from submitting normally
        event.preventDefault();

        // Get some values from elements on the page:
        var $form = $( this ),
            titulo = $form.find( "input[name='titulo']" ).val(),
            descricao = $form.find( "input[name='descricao']" ).val(),
            url = $form.attr( "action" );

        // Send the data using post
        // var posting = $.post( url, { titulo: titulo, descricao:descricao } );

        var posting = $.post( url, $form.serialize());

        // Put the results in a div
        posting.done(function( data ) {
            var response = jQuery.parseJSON(data);
            $.each(response.cards, function(index, item) {
                console.log(item)

            });
            console.log(response.cards.titulo)

            $('.'+response.cards.status).prepend(populaCard(response.cards));
            $form.trigger("reset");
        });
    });

    });

    $( ".drag" ).sortable({
        connectWith : '.boardColumn__taskList',
        items       : '.taskCard',
        placeholder : 'sortable-placeholder'
    });
    $( ".drag" ).disableSelection();
    $( ".drag" ).on( "sortupdate", function( event, ui ) {
        // var $item = $( this );
        // var currentli = $( this ).parent().parent().prop('className');
        // console.log(cardList);
        // console.log(ui.item);
        // console.log(ui);
        // console.log(ui.item.find("div" ).attr('id'))
        if (this === ui.item.parent()[0]) {
            //coluna destino
            console.log(ui.item.parent().parent().attr("id"));
            var novoStatus = ui.item.parent().parent().attr("id");
            var id = ui.item.attr("data-id");
            //id do item
            console.log(ui.item.attr("data-id"));
            atualizaStatus(id, novoStatus);
            // verificaAlteracao();
        }

    } );
    // $( ".drag" ).draggable( "option", "addClasses", true );
</script>