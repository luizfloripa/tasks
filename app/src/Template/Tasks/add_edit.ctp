
<?php echo $this->Form->create($user, [
    'context' => ['validator' => 'Default']
]); ?>

<div class="form-group ">
    <?php echo $this->Form->control('nome', array(
        'class' => 'form-control',
        'label'=> __('Nome:'),
        'placeholder' => __('Informe o nome completo'),
//        'required' => false
    )); ?>
    <?php echo $this->Form->error('nome', null, array('wrap' => 'span', 'class' => 'label label-danger')); ?>
</div>
<p></p>

<div class="form-group ">
    <?php echo $this->Form->control('email', array(
        'class' => 'form-control',
        'label'=> __('E-mail:'),
        'placeholder' => __('Informe o e-mail'),
    )); ?>
    <?php echo $this->Form->error('email', null, array('wrap' => 'span', 'class' => 'label label-danger')); ?>
</div>
<p></p>

<div class="form-group ">
    <?php echo $this->Form->control('password', array(
        'class' => 'form-control',
        'label'=> __('Senha:'),
        'placeholder' => __('Informe a senha'),
        'type' => 'password',
        'required' => false
    )); ?>
    <?php echo $this->Form->error('password', null, array('wrap' => 'span', 'class' => 'label label-danger')); ?>
</div>
<p></p>

<div class="form-group ">
    <?php echo $this->Form->control('confirmacao', array(
        'class' => 'form-control',
        'label'=> __('Confirmar Senha:'),
        'placeholder' => __('Confirme sua senha'),
        'type' => 'password',
        'required' => false
    )); ?>
    <?php echo $this->Form->error('confirmacao', null, array('wrap' => 'span', 'class' => 'label label-danger')); ?>
</div>
<p></p>

<div class="form-group ">
    <?php echo $this->Form->control('ativo', array(
        'class' => 'form-control',
        'label'=> __('Status do usuário'),
        'default' => 'T',
        'options' => array('V' => 'Ativo', 'F' => 'Inativo'),
        'after'=>'</fieldset>'
    )); ?>
    <?php echo $this->Form->error('ativo', null, array('wrap' => 'span', 'class' => 'label label-danger')); ?>
</div>
<p></p>

<p>
    <?php echo $this->Form->submit(__('Salvar'), array(
        'class' => 'btn btn-primary btn-lg',
        'data-loading-text' => __('Enviando...'),
        'id' => 'btn-submit'
    )); ?>
</p>

<?= $this->Html->link(__('Cancelar'), $this->request->referer(),array('class' => 'btn btn-primary btn-lg')) ?>

<?php echo $this->Form->end(); ?>